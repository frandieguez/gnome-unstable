# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Yosef Or Boczko <yoseforb@gnome.org>
# Contributor: Fabian Bornschein <fabiscafe-cat-mailbox-dog-org>
options=(debug !strip)

pkgbase=gnome-software
pkgname=(gnome-software gnome-software-packagekit-plugin)
pkgver=40.0
pkgrel=0.1
pkgdesc="GNOME Software Tools"
url="https://wiki.gnome.org/Apps/Software/"
arch=(x86_64)
license=(GPL2)
depends=(libxmlb gnome-desktop gsettings-desktop-schemas gspell libpackagekit-glib
           gnome-online-accounts appstream appstream-glib libhandy)
makedepends=(appstream-glib gnome-desktop libpackagekit-glib flatpak fwupd 
             docbook-xsl git gobject-introspection gspell gtk-doc meson valgrind
             gnome-online-accounts libxmlb malcontent sysprof)
_commit=d5ef8eff7bc83f8e74e057fec0f3420108f573c8  # tags/40.0^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-software.git#commit=$_commit")
sha256sums=('SKIP')

pkgver() {
  cd $pkgbase
  git describe --tags | sed 's/-/+/g'
}

prepare() {
  cd $pkgbase
}

build() {
  # Ensure static library is non-LTO compatible
  CFLAGS+=" -ffat-lto-objects"

  arch-meson $pkgbase build
  meson compile -C build
}

check() {
  # build container troubles
  meson test -C build --print-errorlogs || :
}

_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

package_gnome-software() {
  groups=(gnome)
  optdepends=('flatpak: Flatpak support plugin'
              'fwupd: fwupd support plugin'
              'malcontent: Parental control plugin')

  DESTDIR="$pkgdir" meson install -C build

### Split gnome-software-packagekit-plugin
  _pick packagekit-plugin "$pkgdir"/usr/lib/gnome-software/plugins-*/libgs_plugin_packagekit*.so
  _pick packagekit-plugin "$pkgdir"/usr/lib/gnome-software/plugins-*/libgs_plugin_systemd-updates.so
}

package_gnome-software-packagekit-plugin() {
  pkgdesc="PackageKit support plugin for GNOME Software"
  depends=(archlinux-appstream-data gnome-software packagekit)
  mv packagekit-plugin/* "$pkgdir"
}
